﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GokuControllerBattle : MonoBehaviour
{
    private int speed = 100;
    private int lives;
    private int balls;
    public Text livesText;
    public Text ballsText;

    void Start()
    {
        lives = 3;
        balls = 0;
        CountBallsAndLives();
    }

    void Update()
    {

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        transform.Translate(0, 0, z);
        transform.Translate(x, 0, 0);

    }

    IEnumerator OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "enemy")
        {
            collider.gameObject.SetActive(false);
            transform.Translate(10, 0, 0);
            transform.Rotate(0, 0, 90, Space.Self);
            lives--;
            yield return new WaitForSeconds(0.5f);
            transform.Rotate(0, 0, -90, Space.Self);
        }
        if (collider.gameObject.tag == "ball")
        {
            balls++;
            collider.gameObject.SetActive(false);
        }

        CountBallsAndLives();
    }

    void CountBallsAndLives()
    {
        livesText.text = "Lives: " + lives.ToString();
        ballsText.text = "Balls: " + balls.ToString();

        if (balls == 7)
        {
            SceneManager.LoadScene("WinScene");
        }
        if (lives == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
