﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SonGokuControllerTraining : MonoBehaviour
{
    private int speed = 100;
    private int lives;
    private int walls;
    public Text livesText;
    public Text wallsText;

    void Start()
    {
        lives = 3;
        walls = 0;
        CountWallsAndLives();
    }

    void Update()
    {

        var x = Input.GetAxis("Horizontal") * Time.deltaTime * speed;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * speed;

        transform.Translate(0, 0, z);
        transform.Translate(x, 0, 0);

        if (Input.GetButtonDown("Jump"))
        {
            Vector3 jump = new Vector3(0.0f, 5 * 100, 0.0f);
            GetComponent<Rigidbody>().AddForce(jump);
        }
    }

    IEnumerator OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "wall")
        {
            var height = GetComponent<Rigidbody>().position.y;
            if (height > 27)
            {
                collider.gameObject.SetActive(false);
                walls++;

            }
            else
            {
                collider.gameObject.SetActive(false);
                transform.Translate(10, 0, 0);
                transform.Rotate(0, 0, 90, Space.Self);
                lives--;
                yield return new WaitForSeconds(0.5f);
                transform.Rotate(0, 0, -90, Space.Self);
            }
            CountWallsAndLives();
        }
    }

    void CountWallsAndLives()
    {
        livesText.text = "Lives: " + lives.ToString();
        wallsText.text = "Walls destroyed: " + walls.ToString();
        if(walls == 10)
        {
            SceneManager.LoadScene("BattleScene");
        }
        if(lives == 0)
        {
            SceneManager.LoadScene("GameOverScene");
        }
    }
}
